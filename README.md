# achats

achats est une application ionic+react rustique de liste de courses basée sur le template 'tabs'

## Installation

1) Après avoir cloné le dépôt distant dans un dossier ~/achats, ouvrir VS Code puis taper :

```bash
npm install
ionic build
```

2) Envoyer le dossier 'build' sur un serveur ftp qui vous apparitient.

## Utilisation

Après avoir chargé la page https://mondomaine/buid/ dans un navigateur, 
cliquer sur 'Liste 1'. Un écran de saisie apparaît.
Il est possible d'ajouter des produits à la liste en cours avec le bouton AJOUTER.
Un produit peut être retiré de la liste grâce aux boutons "Retirer".

## Contribuer
Les contributions par 'Pull requests' sont bienvenues. Pour des suggestions importantes, merci de faire une 'issue' pour discuter de ce que vous voudriez améliorer. 

Assurez-vous que les tests de mise à jour sont valides.

## Licence
CC-BY-SA 3.0