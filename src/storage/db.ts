import { SQLite } from "@ionic-native/sqlite";
import { Produit } from "../model/Produit";
import { isPlatform } from "@ionic/react";
import defaultProducts from "../model/data/produits.json";

var inMemoryProducts = defaultProducts;

const initDBIfNeeded = async () => {
  const db = await SQLite.create({
    name: "dataAchats.db",
    location: "default",
  });
  await db.executeSql(
    "CREATE TABLE IF NOT EXISTS produits(identifiant INTEGER PRIMARY KEY AUTOINCREMENT, titre TEXT, description TEXT, quantite INTEGER, prixUnitaire FLOAT, photo TEXT)",
    []
  );
  return db;
};

export const getProducts = async () => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    return inMemoryProducts;
  }

  const data = await (await initDBIfNeeded()).executeSql(
    "SELECT * FROM produits",
    []
  );
  const retour: Produit[] = [];
  for (let index = 0; index < data.rows.length; index++) {
    const element = data.rows.item(index);
    retour.push(element);
  }
  return retour;
};

export const addProduct = async (produit: Produit) => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    inMemoryProducts = [...inMemoryProducts, produit];
    return inMemoryProducts;
  }

  await (
    await initDBIfNeeded()
  ).executeSql("INSERT INTO produits(titre,description,quantite, prixUnitaire,photo) VALUES(?,?,?,?,?)", [
    produit.titre,
    produit.description,
    produit.quantite,
    produit.prixUnitaire,
    produit.photo,
  ]);

  return getProducts();
};
