import React, { useState, useEffect } from "react";
import "./ExploreContainer.css";
import Produits from "./Achats";
import AjoutProduit from "./AjoutProduit";
import CalculSomme from "./CalculSomme";
import SauverProduits from "./SauverProduits";
import { getProducts, addProduct } from "../storage/db";
import { Produit } from "../model/Produit";

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {
  const [produits, setProduits] = useState<Produit[]>([]);

  useEffect(() => {
    getProducts().then((res) => setProduits(res));
  }, []);

  return (
    <>
      <AjoutProduit nouveauProduit={(produit) => addProduct(produit).then(setProduits)} />
      <SauverProduits produits={produits}></SauverProduits>
      <br></br>
      <CalculSomme produits={produits}></CalculSomme>
      <Produits produits={produits}></Produits>
    </>
  );
};

export default ExploreContainer;
