import React from "react";
import { Produit } from "../model/Produit";
import { IonList, IonItem, IonLabel, IonAvatar, IonText, IonButton } from "@ionic/react";

interface Props {
  produits: Produit[];
}

/* Ce code n'est pas fonctionnel, je l'ai pris sur dépôt github
A adapter au cas présent.
L'idée c'est que quand on appuie sur Sauver, on veut écrire
le tableau "produits: Produit[]" dans le fichier json
../model/data/produits/json

Pour y réfléchir :
1) https://leblogducodeur.fr/ecrire-et-lire-des-fichiers-json-avec-node-js/
2) https://leblogducodeur.fr/lasynchronisme-en-programmation/
3) https://nodejs.org/api/fs.html

const fs = require('fs')

function readFileAsync(file: string) {
    return new Promise((res, rej) => {
        fs.readFile(file, 'utf8', (err: number, content: string) => {
            if (err) {
                return rej(err)
            }
            res(content)
        })
    })
}

function writeFileAsync(file: string, data: Produit[]) {
    return new Promise((res, rej) => {
        fs.writeFile(file, data, 'utf8', (err: number) => {
            if (err) {
                return rej(err)
            }
            res(true)
        })
    })
}


async function readJsonFile(file: string) {
    const contentFile :string= await readFileAsync(file);
    return JSON.parse(contentFile);
}

async function writeJsonFile(file: string, data: Produit[]) {
  await writeFileAsync(file, JSON.stringify(data))
}

readJsonFile('../model/data/produits.json')
    .then((content) => {
        console.log(content)
        if (content.messages === undefined) {
            content.messages = []
        }

        content.messages.push({
            pseudo: 'test',
            content: 'Contenu du message numéro ' + (content.messages.length + 1)
        })

        return writeJsonFile('../model/data/produits.json', content)
    })
    .then(() => {
        console.log('File updated')
    })
    .catch((err) => {
        console.error(err)
    })

*/

const SauverProduits = ({ produits }: Props) => (
    <IonButton color="success" onClick={()=>console.log("Sauver")}>
        Sauver
    </IonButton>
);

export default SauverProduits;