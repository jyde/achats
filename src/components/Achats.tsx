import React from "react";
import { Produit } from "../model/Produit";
import { IonList, IonItem, IonLabel, IonAvatar, IonButton } from "@ionic/react";

interface Props {
  produits: Produit[];
}

const Produits = ({ produits }: Props) => (
  <IonList>
    {produits.map((produit) => (
      <IonItem key={produit.titre}>
        <IonLabel>
          {produit.photo && (
            <IonAvatar slot="start">
              <img alt="" src={`data:image/png;base64, ${produit.photo}`} />
            </IonAvatar>
          )}
          <h2>{produit.titre}</h2>
          <h3>{produit.description}</h3>
          <span id="qte">{produit.quantite}</span> fois <span id="PU">{produit.prixUnitaire}</span> €
          <br />
          <IonButton color="warning" onClick={()=>produits.splice(produits.indexOf(produit),1)}>Retirer</IonButton>
        </IonLabel>
      </IonItem>
    ))}
  </IonList>
);

export default Produits;
