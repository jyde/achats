import React from "react";
import { Produit } from "../model/Produit";
import { IonList, IonItem, IonLabel, IonAvatar, IonText } from "@ionic/react";

interface Props {
  produits: Produit[];
}

const reducer = (accumulator: number, currentValue: number) => accumulator + currentValue;

const CalculSomme = ({ produits }: Props) => (
    <IonText>
        <br></br>
        Montant total : {produits
            .map((produit) => (produit.quantite*produit.prixUnitaire))
            .reduce(reducer,0)
            .toFixed(2)}
        <br></br>
    </IonText>
);

export default CalculSomme;
