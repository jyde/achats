import React, { useState } from "react";
import {
  IonInput,
  IonTextarea,
  IonButton,
  IonIcon,
  IonAvatar,
} from "@ionic/react";
import { Produit } from "../model/Produit";
import { camera } from "ionicons/icons";
import { CameraResultType } from "@capacitor/core";
import { useCamera } from "@ionic/react-hooks/camera";

interface Props {
  nouveauProduit: (produit: Produit) => void;
}



const AjoutProduit = ({ nouveauProduit }: Props) => {
  const [titre, setTitre] = useState<string>();
  const [description, setDescription] = useState<string>();
  const [quantite, setQuantite] = useState<number>();
  const [prixUnitaire, setPU] = useState<number>();
  const { photo, getPhoto } = useCamera();
  if (quantite!=undefined && 
    quantite>0 && 
    prixUnitaire != undefined && 
    prixUnitaire>0 && 
    titre !="" &&
    description !="") {
  /* Verifications */
    return (
      <>
        <IonIcon
          icon={camera}
          onClick={() => {
            getPhoto({
              quality: 100,
              allowEditing: false,
              resultType: CameraResultType.Base64,
            }).then((tof) => console.log(tof));
          }}
        ></IonIcon>
        <div>
          {photo && (
            <IonAvatar slot="start">
              <img alt="" src={`data:image/png;base64, ${photo.base64String}`} />
            </IonAvatar>
          )}
        </div>
        <IonInput
          placeholder="Titre"
          onIonChange={(e) => setTitre(e.detail.value!)}
        ></IonInput>
        <IonTextarea
          placeholder="Description"
          onIonChange={(e) => setDescription(e.detail.value!)}
        ></IonTextarea>
        <IonInput type="number"
          placeholder="Quantité, ex: 1"
          onIonChange={(e) => setQuantite(parseInt(e.detail.value!))}
        ></IonInput>
        <IonInput type="number"
          placeholder="Prix unitaire, ex: 1.50"
          onIonChange={(e) => setPU(parseFloat(e.detail.value!))}
        ></IonInput>
        <IonButton
          onClick={() =>
            nouveauProduit({
              titre: titre!,
              description: description!,
              quantite: quantite!,
              prixUnitaire: prixUnitaire!,
              photo: photo?.base64String,
            })
          }
        >
          Ajouter
        </IonButton>
      </>
    );
  } else {
    return (
      <>
        <IonIcon
          icon={camera}
          onClick={() => {
            getPhoto({
              quality: 100,
              allowEditing: false,
              resultType: CameraResultType.Base64,
            }).then((tof) => console.log(tof));
          }}
        ></IonIcon>
        <div>
          {photo && (
            <IonAvatar slot="start">
              <img alt="" src={`data:image/png;base64, ${photo.base64String}`} />
            </IonAvatar>
          )}
        </div>
        <IonInput
          placeholder="Titre"
          onIonChange={(e) => setTitre(e.detail.value!)}
        ></IonInput>
        <IonTextarea
          placeholder="Description"
          onIonChange={(e) => setDescription(e.detail.value!)}
        ></IonTextarea>
        <IonInput type="number"
          placeholder="Quantité, ex: 1"
          onIonChange={(e) => setQuantite(parseInt(e.detail.value!))} /* parseInt(e.detail.value!) */
        ></IonInput>
        <IonInput type="number"
          placeholder="Prix unitaire, ex: 1.50"
          onIonChange={(e) => setPU(parseFloat(e.detail.value!))} /* parseFloat(e.detail.value!) */
        ></IonInput>
        <IonButton
          onClick={() =>
            nouveauProduit({
              titre: titre!,
              description: description!,
              quantite: quantite!,
              prixUnitaire: prixUnitaire!,
              photo: photo?.base64String,
            })
          }
        >
          Ajouter
        </IonButton>
      </>
    );

  };
};

export default AjoutProduit;
