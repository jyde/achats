// import { StringDecoder } from "string_decoder";

export interface Produit {
  titre: string;
  description: string;
  quantite: number;
  prixUnitaire: number;
  photo?: string;
}
